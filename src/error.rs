use snafu::Backtrace;
use actix_web::error::ResponseError;

use crate::db;

#[derive(Debug, Snafu)]
#[snafu(visibility = "pub(crate)")]
pub enum ShaftError {
    #[snafu(display("{}", source))]
    DatabaseError {
        source: db::DatabaseError,
        backtrace: Backtrace,
    },
}

impl ResponseError for ShaftError {}
