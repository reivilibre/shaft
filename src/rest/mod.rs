//! Handles all REST endpoints

use std::sync::Arc;

use actix_web::web::ServiceConfig;
use chrono;
use handlebars;
use num_format::{CustomFormat, Grouping, ToFormattedString};
use serde_json;

use crate::db;
use crate::HttpClient;

pub use self::auth::{AuthenticatedUser, AuthenticateUser};
pub use self::logger::MiddlewareLogger;

mod api;
mod auth;
mod login;
mod logger;
mod static_files;
mod web;

/// Registers all servlets in this module with the HTTP app.
pub fn register_servlets(config: &mut ServiceConfig, state: &AppState) {
    login::register_servlets(config);
    api::register_servlets(config);
    static_files::register_servlets(config, state);
    web::register_servlets(config)
}

// Holds the state for the shared state of the app. Gets cloned to each thread.
#[derive(Clone)]
pub struct AppState {
    pub database: Arc<dyn db::Database>,
    pub config: AppConfig,
    pub cpu_pool: futures_cpupool::CpuPool,
    pub handlebars: Arc<handlebars::Handlebars>,
    pub http_client: HttpClient,
}

/// Read only config for the app
#[derive(Clone)]
pub struct AppConfig {
    pub web_root: String,
    pub resource_dir: String,
}

/// Formats the current time plus two years into a cookie expires field.
pub fn get_expires_string() -> String {
    let dt = chrono::Utc::now() + chrono::Duration::weeks(104);
    const ITEMS: &[chrono::format::Item<'static>] =
        &[chrono::format::Item::Fixed(chrono::format::Fixed::RFC2822)];
    dt.format_with_items(ITEMS.iter().cloned()).to_string()
}

/// Format yen into a pretty yen string
fn format_yen(yen: i64) -> String {
    let format = CustomFormat::builder()
        .grouping(Grouping::Standard)
        .separator("'")
        .build().expect("Cannot make CustomFormat");

    yen.to_formatted_string(&format) + "¥"
}

/// Handlebars helper function for formatting yen.
pub fn yen_formatting_helper(
    h: &handlebars::Helper,
    _: &handlebars::Handlebars,
    _: &handlebars::Context,
    _: &mut handlebars::RenderContext,
    out: &mut dyn handlebars::Output,
) -> Result<(), handlebars::RenderError> {
    let param = h.param(0).unwrap();

    match *param.value() {
        serde_json::Value::Number(ref number) => {
            let yen = number
                .as_i64()
                .ok_or_else(|| handlebars::RenderError::new("Param must be a number"))?;
            out.write(&format_yen(yen))?;
            Ok(())
        }
        _ => Err(handlebars::RenderError::new("Param must be a number")),
    }
}

/// The body of a incoming request shaft the given user.
#[derive(Deserialize)]
struct ShaftUserBody {
    /// The other party in the transaction.
    other_user: String,
    /// The amount in yen owed. Positive means shafter is owed money by other
    /// user, negative means shafer owes money.
    amount: i64,
    /// The human readable description of the transasction.
    reason: String,
}
