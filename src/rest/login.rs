//! Handles login flow using Github OAuth.

use actix_web::web::ServiceConfig;
use actix_web::{web, Error, HttpResponse, error};
use futures::{Future, IntoFuture};
use hyper;

use crate::rest::{get_expires_string, AppState};

/// Register servlets with HTTP app
pub fn register_servlets(config: &mut ServiceConfig) {
    config.route("/login", web::get().to_async(authenticate));
}

#[derive(Deserialize)]
struct LoginRequest {
    /// The token that we should keep.
    #[serde(default)]
    token: Option<String>,
}

/// Handles inbound `/login` request that includes the user's token.
fn authenticate(
    (query, state): (web::Query<LoginRequest>, web::Data<AppState>),
) -> Box<dyn Future<Item=HttpResponse, Error=Error>> {
    let db = state.database.clone();
    let hb = state.handlebars.clone();

    let web_root = state.config.web_root.clone();

    match query.token.clone() {
        None => {
            let rendered = hb.render("login_failure", &json!({}))
                .unwrap_or_else(|e| format!("Rendering Error (login_failure) {:?}", e));
            Box::new(HttpResponse::Unauthorized()
                .content_length(rendered.len() as u64)
                .body(rendered).into_future())
        },
        Some(token) => {
            let f1 = db.get_user_from_token(token.clone());

            let f2 = f1.map_err(error::ErrorInternalServerError).and_then(move |user_opt| {
                match user_opt {
                    Some(user) => {
                        let rendered = hb.render("login_success", &json!({"display_name": &user.display_name,}))
                            .unwrap_or_else(|e| format!("Rendering Error (login_success) {:?}", e));
                        HttpResponse::Ok()
                            .header(
                                hyper::header::SET_COOKIE,
                                format!(
                                    "token={}; HttpOnly; Secure; Path=/; Expires={}; SameSite=lax",
                                    token,
                                    get_expires_string(),
                                ),
                            )
                            .header(hyper::header::LOCATION, web_root)
                            .content_length(rendered.len() as u64)
                            .body(rendered)
                    }
                    None => {
                        let rendered = hb.render("login_failure", &json!({}))
                            .unwrap_or_else(|e| format!("Rendering Error (login_failure) {:?}", e));
                        HttpResponse::Unauthorized()
                            .content_length(rendered.len() as u64)
                            .body(rendered)
                    }
                }
            });

            Box::new(f2.into_future())
        },
    }

}
