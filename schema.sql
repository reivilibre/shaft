
CREATE TABLE IF NOT EXISTS tokens (
	user_id TEXT NOT NULL,
	token TEXT NOT NULL PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS users (
	user_id TEXT NOT NULL PRIMARY KEY,
	display_name TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS transactions (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	shafter TEXT NOT NULL REFERENCES users (user_id),
	shaftee TEXT NOT NULL REFERENCES users (user_id),
	amount INT NOT NULL,
	time_sec INT NOT NULL,
	reason TEXT NOT NULL
);

CREATE INDEX IF NOT EXISTS transactions_shafter ON transactions (shafter);
CREATE INDEX IF NOT EXISTS transactions_shaftee ON transactions (shaftee);
