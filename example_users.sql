INSERT INTO users (user_id, display_name)
	VALUES ('u1', 'User One'), ('u2', 'User Two');

INSERT INTO tokens (token, user_id)
	VALUES ('t1', 'u1'), ('t2', 'u2');
